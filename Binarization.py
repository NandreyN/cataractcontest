import cv2
from PIL import Image
from matplotlib.pyplot import imshow, show, title
from numpy.ma import array
from skimage.filters import rank
from skimage.morphology import disk

def otsu(im, sigma):
    im = array(im)
    #im = cv2.cvtColor(im, cv2.COLOR_BGR2GRAY)
    blur = cv2.GaussianBlur(im, (sigma, sigma), 0)
    bin, th = cv2.threshold(blur, 0, 255, cv2.THRESH_BINARY + cv2.THRESH_OTSU)
    return th


def adaptive(im,sigma, r,c):
    im = array(im)
    im = cv2.GaussianBlur(im, (sigma, sigma), 0)
    #im = cv2.cvtColor(im, cv2.COLOR_BGR2GRAY)
    th = cv2.adaptiveThreshold(im, 255, cv2.ADAPTIVE_THRESH_GAUSSIAN_C ,cv2.THRESH_BINARY,r,c)
    return th

def threshold(im, t):
    im = array(im)
    bin, th = cv2.threshold(im, t, 255, cv2.THRESH_BINARY)
    return th

