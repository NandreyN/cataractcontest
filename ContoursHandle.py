import cv2
import numpy as np
import os
from ImageDifference import calcDiff, Surface, Region


'''def drawAngles(img):
    # image is binary
    im2, contours, hierarchy = cv2.findContours(img, 1, 2)
    shp = (img.shape[1], img.shape[0])
    img = cv2.cvtColor(img, cv2.COLOR_GRAY2BGR);
    cont = []

    for c in contours:
        M = cv2.moments(c)
        if M["m00"] and len(c) >= 5:
            (x, y), (MA, ma), angle = cv2.fitEllipse(c)
            cX = int(M["m10"] / M["m00"])
            cY = int(M["m01"] / M["m00"])

            dx = int(np.sin(-angle * np.pi / 180) * 100)
            dy = int(np.cos(-angle * np.pi / 180) * 100)
            cont.append((cX, cY, angle, c))

            cv2.line(img, (cX, cY), (cX + dx, cY + dy), (255, 0, 0), 1)

    surface = Surface(shp, 2, 5)

    list(map(lambda c: surface.addPoint(c[0], c[1], c[2]), cont))
    pt = surface.choosePoint()
    if pt == None: return img

    c = [x for x in cont if x[0] == pt[0] and x[1] == pt[1]][0]
    pt = c

    region = Region(maxDistInRegion=shp[1] // 3, contour=c, maxAngleDifference=90)
    assert len(cont) > 0

    list(map(lambda x: region.add(x), cont))
    c = region.getColletion()
    for elem in c:
        dx = int(np.sin(-elem[2] * np.pi / 180) * 100)
        dy = int(np.cos(-elem[2] * np.pi / 180) * 100)

        cv2.line(img, (elem[0], elem[1]), (elem[0] + dx, elem[1] + dy), (0, 0, 255), 1)

    dx = int(np.sin(-pt[2] * np.pi / 180) * 100)
    dy = int(np.cos(-pt[2] * np.pi / 180) * 100)

    cv2.line(img, (pt[0], pt[1]), (pt[0] + dx, pt[1] + dy), (0, 255, 0), 1)
    return img


newDimension = (960, 480)
thresholdDifference = 20
fps = 20
kernel = np.ones((5, 5), np.float32) / 25
morphKernel = cv2.getStructuringElement(cv2.MORPH_ELLIPSE, (5, 5))
outputDirectory = r"E:\Program Files\CataractContest\Angles"

cap = cv2.VideoCapture(r"C:\Users\andre\PycharmProjects\ImageTest1\video\video_cut_005.avi")
cap.set(cv2.CAP_PROP_FPS, fps)

assert cap.isOpened()

ret, first = cap.read()

assert ret
first = cv2.cvtColor(first, cv2.COLOR_BGR2GRAY)
first = cv2.resize(first, newDimension)
first = cv2.filter2D(first, -1, kernel)

ret, second = cap.read()
assert ret

i = 0
while ret:
    # Convert frame to grayscale and resize
    originalImageSrc = second.copy()

    second = cv2.cvtColor(second, cv2.COLOR_BGR2GRAY)
    second = cv2.resize(second, newDimension)
    second = cv2.filter2D(second, -1, kernel)

    # Calculate difference image
    diffImage = calcDiff(first, second, thresholdDifference)
    diffImage = cv2.morphologyEx(diffImage, cv2.MORPH_OPEN, morphKernel)

    diffImage = drawAngles(diffImage);

    name = "im" + str(i) + ".png"
    path = os.path.join(outputDirectory, name)

    print(path)

    cv2.imwrite(path, diffImage)

    first = second.copy()
    ret, second = cap.read()
    i += 1
cap.release()'''
