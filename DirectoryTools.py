import re
from os import listdir
from os.path import join, isfile

def enumerateFiles(folderPath):
    return [f for f in listdir(folderPath) if isfile(join(folderPath, f))]

def enumerateFilesFullPath(folderPath):
    return [join(folderPath,f) for f in listdir(folderPath) if isfile(join(folderPath, f))]

def enumerateFilesSort(folderPath):
    lst = enumerateFiles(folderPath)
    lst.sort(key=lambda x: filter(x), reverse=False)
    return lst

def enumerateFilesRange(folderPath, start,end):
    sortedList = enumerateFilesSort(folderPath)
    if int(start) < 1 or int(end) > len(sortedList): raise IndexError
    return [f for f in  sortedList if int(start) <= int(filter(f)) <= int(end)]

def filter(string):
    match = re.findall(r'(\d{1,15})(\.)', string)
    ret = 0
    if match is not None: ret = int(match[0][0])
    return int(ret)