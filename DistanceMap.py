import datetime
from math import sqrt

import cv2
import numpy
import skimage
from skimage import *
from PIL import Image
from matplotlib.pyplot import imshow, show, imsave, figure
from numpy import array, zeros
from skimage import io, color
from skimage import img_as_float

matrixBuffer = ()
available = 0

def checkIndex(coord, X, Y):
    return coord[0] > 0 and coord[1] > 0 and coord[0] < X and coord[1] < Y


def dist(point1, point2):
    """
    Calculates Euclidean distance between two points
    :param point1:coordinates of the 1st point
    :param point2:coordinates of the 2nd point
    :return: float distance
    """
    return sqrt((point1[0] - point2[0]) ** 2 + (point1[1] - point2[1]) ** 2)


def neighbourhood_intensity(fragment):
    central = fragment[1, 1]
    res = numpy.sum(fragment.flatten()) - central + 1
    if res < 0: raise ValueError
    return res


def metrics(value, centre, i, shp):
    """
    Calculates metrics for each pixel of the flatten image
    :param value: pixel value
    :param centre: central pixel (x,y)
    :param i: current pixel index
    :param shp: image width
    :return: returns float value
    """
    global matrixBuffer
    global available
    if available == 0: raise Exception

    # check indexes
    if not checkIndex(i, *shp): return 0.

    subMatrix = matrixBuffer[i[0] - 1:i[0] + 2, i[1] - 1:i[1] + 2]
    nei = neighbourhood_intensity(subMatrix)
    eu_dist = dist(i, centre)

    return float(0.75 * eu_dist / shp[0] * 2 + 0.25 * 8 * 255 / nei) / 1000


def createMap(img):
    """
    Returns float image , which pixel is treated as distance between current pixel and current one
    :param img: image
    :return: map
    """
    global matrixBuffer
    global available

    img = array(img)
    opencvImage = cv2.cvtColor(img, cv2.COLOR_RGB2GRAY)
    opencvImage = cv2.equalizeHist(opencvImage)
    img = img_as_float(opencvImage)

    new = numpy.pad(img, ((1, 1), (1, 1)), mode="constant", constant_values=0.)
    centre = (new.shape[0] // 2, new.shape[1] // 2)

    matrixBuffer = new
    available = 1

    flat = new.ravel()
    resultMatrix = array(list(map(lambda args: metrics(args[1], centre, args[0], new.shape),
                                        numpy.ndenumerate(new)))).reshape(new.shape)

    available = 0

    resultMatrix = img_as_ubyte(resultMatrix)
    return resultMatrix
