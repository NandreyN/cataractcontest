import cv2
import numpy as np
import time


def paintOverHighlights(image):
    """
    Function is intended for detecting highlights at the image
    :param image: opencv BGR format image
    :return: BGR image
    """
    originalImageCopy = image.copy()
    image = cv2.cvtColor(image, cv2.COLOR_BGR2HSV)

    image = cv2.medianBlur(image, 11)

    lower_border = np.array([0, 0, 230], dtype=np.uint8)
    upper_border = np.array([0, 0, 255], dtype=np.uint8)

    mask = cv2.inRange(image, lower_border, upper_border)
    result = cv2.bitwise_and(image, image, mask=mask)
    mask = cv2.cvtColor(result, cv2.COLOR_HSV2BGR)
    mask = cv2.cvtColor(mask, cv2.COLOR_BGR2GRAY)

    # now we have binary mask for highlights
    image = cv2.cvtColor(image, cv2.COLOR_HSV2BGR)
    image = cv2.cvtColor(image, cv2.COLOR_BGR2GRAY)

    # average value of pixel to cover light spot is calculated
    # work with mask next
    # detect contours and get bounding rectangles

    im2, contours, hierarchy = cv2.findContours(mask, 1, 2)

    ratio = 15
    for cnt in contours:
        x, y, w, h = cv2.boundingRect(cnt)
        w *= ratio
        h *= ratio
        x -= w // 2
        y -= h // 2

        blurG = cv2.GaussianBlur(image, (15, 15), 0)
        image[x:x + w, y:y + h] = blurG[x:x + w, y:y + h]
    # color that rectangles on the original grayscale image using average pixel value
    image = cv2.cvtColor(image, cv2.COLOR_GRAY2BGR)
    return image


def impaintHighlights(image, windowSize = 5, lowBorder = 180):
    image = cv2.cvtColor(image, cv2.COLOR_BGR2HSV)
    lower_border = np.array([0, 0, lowBorder], dtype=np.uint8)
    upper_border = np.array([0, 0, 255], dtype=np.uint8)

    mask = cv2.inRange(image, lower_border, upper_border)
    result = cv2.bitwise_and(image, image, mask=mask)
    mask = cv2.cvtColor(result, cv2.COLOR_HSV2BGR)
    mask = cv2.cvtColor(mask, cv2.COLOR_BGR2GRAY)

    image = cv2.cvtColor(image, cv2.COLOR_HSV2BGR)
    dst = cv2.inpaint(image, mask, windowSize,cv2.INPAINT_TELEA)

    return dst

'''videoPath = "video/video_cut_005.avi"
cap = cv2.VideoCapture(videoPath)

video = cv2.VideoWriter('impaint_window=55Low=180.avi', cv2.VideoWriter_fourcc(*'DIVX'), 20.0, (960, 480), isColor=True)
i = 0

assert (cap.isOpened())
ret, frame = cap.read()
i = 0
while ret:
    a = impaintHighlights(frame,windowSize=55,lowBorder=180)
    video.write(a)
    ret, frame = cap.read()
    print("Frame ", i)
    i += 1

print("Finish")
cap.release()
video.release()'''
