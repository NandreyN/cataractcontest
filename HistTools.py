import glob
from os import listdir
from os.path import join, isfile
import os
from PIL import Image
from pylab import *
import numpy as np
import DirectoryTools as dt
import matplotlib.pyplot as plt
from operator import add


def getAvHistForFolder(folderPath):
    files = dt.enumerateFiles(folderPath)
    globHist = [[0] * 256, [0] * 256, [0] * 256]
    edges = [[0] * 257, [0] * 257, [0] * 257]
    for file in files:
        currentImage = array(Image.open(join(folderPath, file)))

        for i in range(3):
            sliced = currentImage[:, :, i]
            histog, e = histogram(sliced.flatten(), bins=256)

            edges[i] = [sum(x) for x in zip(edges[i], e)]
            globHist[i] = [sum(x) for x in zip(globHist[i], histog)]

    for i in range(3):
        globHist[i] = [x // len(files) for x in globHist[i]]
        edges[i] = [x / len(files) for x in edges[i]]
    return globHist, edges


def showHistogram(histogramData, edges):
    plt.bar(arange(256), histogramData, align='center')
    plt.show()


def saveHistogram(histogramData, chanelID, outPath):
    plt.bar(arange(256), histogramData, align='center')
    path = join(outPath, "Hist_ChanelId = " + str(chanelID) + ".png")
    title("Hist_ChanelId = " + str(chanelID))
    savefig(path)
    plt.gcf().clear()


def concatImageHistograms(image):
    data = []
    im = array(Image.open(image))
    for i in range(3):
        h, e = histogram(im[:, :, i], 256)
        data += list(h)
    return data


def getImageHist(image):
    """
    :param image: grayscale image
    :return: histogram as array
    """
    h, e = histogram(image, bins=256) # h = histogram
    return h

