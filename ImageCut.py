import os
from os import listdir
from os.path import join, isfile,exists
from pylab import *
from PIL import Image

import DirectoryTools


def cutImage(path,outFolderPath,imageId,dimX = 32, dimY = 32, maxImages = 50):
    baseImage = np.array(Image.open(path))
    X = baseImage.shape[0]
    Y = baseImage.shape[1]
    if (X < dimX or Y < dimY):
        raise AttributeError

    count = 0
    for i in range(X // dimX):
        for j in range(Y // dimY):
            cut = baseImage[i*dimX:(i+1)*dimX,j*dimY:(j+1)*dimY]
            name = "id" + str(imageId) + str(i) + "__" + str(j) + ".png"
            if (not exists(outFolderPath)):
                os.makedirs(outFolderPath)
            imsave(join(outFolderPath, name), cut)
            count += 1
            if (count >= maxImages): return

def cutImagesInFolder(folderPath, outPath, dimX = 32, dimY = 32, maxImages = 50):
    files = DirectoryTools.enumerateFiles(folderPath)
    id = 0
    for file in files:
        fullPath = join(folderPath, file)
        print("Cutting :", fullPath)
        cutImage(fullPath,outPath,id,dimX,dimY, maxImages)
        id += 1