from distutils.command.config import config

import numpy as np
import cv2
import sys
from os.path import join
from DirectoryTools import enumerateFiles
from DistanceMap import dist
from HighlightHandler import paintOverHighlights, impaintHighlights
from borderDetectors import sobel, canny
import diff
import time


def calcDiff(im1, im2, threshold):
    """
    :param im1: grayscale image1
    :param im2: grayscale image2
    :param threshold: threshold for pixel value difference
    :return: binary diff image
    """

    # padding with zeros for handling edge values
    def func(args):
        x = args[0]
        y = args[1]
        # check index
        if 0 < x < im1.shape[0] and 0 < y < im1.shape[1]:
            env1 = im1[x - 1:x + 2, y - 1:y + 2]
            env2 = im2[x - 1:x + 2, y - 1:y + 2]
            env1Av = np.sum(env1, axis=(0, 1)) / pow(3, 2)
            env2Av = np.sum(env2, axis=(0, 1)) / pow(3, 2)
            return np.int8(0 if abs(env1Av - env2Av) <= threshold else 255)
        else:
            return 0

    diffImage = np.zeros_like(im1)
    h, w = im1.shape
    for i in range(h):
        for j in range(w):
            diffImage.itemset((i, j), func((i, j)))
    # diffImage = np.array(list(map(lambda args: func(args), np.ndenumerate(im1)))).reshape(im1.shape).astype(np.uint8)
    return diffImage


def handleVideo(path, threshold, name="", dim=(480, 240)):
    cap = cv2.VideoCapture(path)
    if not cap.isOpened(): return
    print("Total frames : ", int(cap.get(cv2.CAP_PROP_FRAME_COUNT)))
    ret, prevFrame = cap.read()

    prevFrame = cv2.cvtColor(prevFrame, cv2.COLOR_BGR2GRAY)
    if prevFrame.size != dim: prevFrame = cv2.resize(prevFrame, dim)

    video = cv2.VideoWriter('diffBin_t' + str(threshold) + name + '.avi', cv2.VideoWriter_fourcc(*'DIVX'), 20.0, dim,
                            isColor=False)
    counter = 0

    while ret:
        try:
            ret, nextFrame = cap.read()
        except Exception:
            cap.release()
            video.release()
            return

        nextFrame = cv2.cvtColor(nextFrame, cv2.COLOR_BGR2GRAY)
        if nextFrame.size != dim: nextFrame = cv2.resize(nextFrame, dim)

        im = calcDiff(prevFrame, nextFrame, threshold)
        video.write(im)

        prevFrame = nextFrame.copy()
        counter += 1
        print("Handled ", counter)

    video.release()
    cap.release()


class Rectangle:
    def __init__(self, x, y, w, h):
        self._x = x
        self._y = y
        self._w = w
        self._h = h
        self.__contours = []

    def inRange(self, contour):
        return True if self._x <= contour["cx"] <= self._x + self._w and \
                       self._y <= contour["cy"] <= self._y + self._h else False;

    def add(self, contour):
        self.__contours.append(contour)

    def getMaxScatter(self):
        max = np.max([x["angle"] for x in self.__contours])
        min = np.min([x["angle"] for x in self.__contours])
        return max - min;

    def getAvAngle(self):
        return sum([x["angle"] for x in self.__contours]) // len(self.__contours) if len(self.__contours) != 0 else 0

    def getSumArea(self):
        return np.sum([x["area"] for x in self.__contours])

    def getWeight(self):
        if len(self.__contours) == 0: return 0.
        return 0.2 * self.getMaxScatter() + 0.7 * self.getSumArea() + 0.1 * (len(self.__contours) + 50)

    def choosePoint(self):
        if len(self.__contours) == 0: return None;

        avAngle = self.getAvAngle()
        (centreX, centreY) = (self._x + self._w // 2, self._y + self._h // 2)

        distances = [dist((centreX, centreY), (x["cx"], x["cy"])) for x in self.__contours]
        minDist = min(distances)

        collection = zip(distances, self.__contours)
        elem = next((x[1] for x in collection if minDist == x[0]), self.__contours[0])

        return elem

    def visualize(self, img):
        cv2.line(img, (self._y, self._x), (self._y + self._h, self._x), (255, 0, 0))
        cv2.line(img, (self._y + self._h, self._x), (self._y + self._h, self._x + self._w), (255, 0, 0))
        cv2.line(img, (self._y + self._h, self._x + self._w), (self._y, self._x + self._w), (255, 0, 0))
        cv2.line(img, (self._y, self._x + self._w), (self._y, self._x), (255, 0, 0))

        return img

    def getCoordParams(self):
        return self._x, self._y, self._w, self._h


class Field:
    def __init__(self, iterable, shape, X, Y):
        self.__contoursData = iterable
        self.__rectangles = []

        # initializing rectangles collection
        perX, perY = shape[0] // X, shape[1] // Y  # error?
        for i in range(X):
            for j in range(Y):
                cornerX = i * perX
                cornerY = j * perY
                self.__rectangles.append(Rectangle(cornerX, cornerY, perX, perY))

        for cont in self.__contoursData:
            for rect in self.__rectangles:
                if rect.inRange(cont):
                    rect.add(cont)
                    break;

    def choosePoint(self):
        lst = [x.getWeight() for x in self.__rectangles]
        maximum = max(lst)

        coll = zip(lst, self.__rectangles)
        chRectangle = [x[1] for x in coll if x[0] == maximum]
        if len(chRectangle) == 0: raise ValueError("Maximum issue");
        return chRectangle[0].choosePoint()

    def getInstrumentPointCollection(self, deltaAngle, radius):
        srcPoint = self.choosePoint()
        if srcPoint is None: return None
        points = [x for x in self.__contoursData if
                  dist((x["cx"], x["cy"]), (srcPoint["cx"], srcPoint["cy"])) <= radius and
                  abs(srcPoint["angle"] - x["angle"]) <= deltaAngle]
        return points

    def visual(self, img):
        for r in self.__rectangles:
            r.visualize(img)
        return img


class RectangleController:
    def __init__(self, maxShift, maxAreaRelativeDiff=2, rectangle=None):
        self._maxShift = maxShift
        self._currentRectangle = rectangle
        self._maxAreaDiff = maxAreaRelativeDiff
        pass

    # noinspection PyMethodMayBeStatic
    def _getCentre(self, rectangle):
        """
        :param rectangle: Rectangle class object
        :return:(x,y) rectangle centre tuple
        """
        params = rectangle.getCoordParams()
        return params[0] + params[2] // 2, params[1] + params[3] // 2

    # noinspection PyMethodMayBeStatic
    def _getArea(self, rectangle):
        if rectangle is None: return 0

        p = rectangle.getCoordParams()
        return p[2] * p[3]

    def _getAreaRelativeDiff(self, rectangle1, rectangle2):
        try:
            res = self._getArea(rectangle1) / self._getArea(rectangle2)
        except ZeroDivisionError:
            res = 0

        return res

    def _getShift(self, rectangle):
        """
        :param rectangle: Rectangle class object
        :return: shift (dx,dy) of current rectangle relatively to the current one
        """
        centreCurrent = self._getCentre(self._currentRectangle)
        paramCentre = self._getCentre(rectangle)
        return centreCurrent[0] - paramCentre[0], centreCurrent[1] - paramCentre[1]

    def passRectangle(self, rectangle, compulsory=False):
        """
        :param rectangle: Rectangle class object
        :param compulsory: Bool, if necessary to set rectangle compulsory
        :return: void
        """
        if compulsory or self._currentRectangle is None or rectangle is None:
            self._currentRectangle = rectangle
            return

        shift = self._getShift(rectangle)
        rdif = max(self._getAreaRelativeDiff(self._currentRectangle, rectangle),
                   self._getAreaRelativeDiff(rectangle, self._currentRectangle))

        if rdif == 0: return

        if max(abs(shift[0]), abs(shift[1])) <= self._maxShift and rdif <= self._maxAreaDiff:
            self._currentRectangle = rectangle

    def getCurrentRectangle(self):
        """
        Getter
        :return: current Rectangle class object
        """
        return self._currentRectangle


def medianAreaFilter(contours):
    areas = []
    for cnt in contours:
        areas.append(cv2.contourArea(cnt))
    areasZip = list(zip(areas, contours))

    sorted(areasZip, key=lambda x: x[0])
    median = np.median(areas)

    outCnt = []
    for (a, cnt) in areasZip:
        if a <= median: outCnt.append(cnt)

    return outCnt


def getInstrumentArea(dif):
    # need to form vector (cX,cY,area,angle)
    im2, contoursH, hierarchy = cv2.findContours(dif, 1, 2)
    if len(contoursH) != 0:
        contoursH = medianAreaFilter(contoursH)

    propertyCollection = []
    for cnt in contoursH:
        M = cv2.moments(cnt)
        if M['m00'] != 0 and len(cnt) >= 5:
            cx = int(M['m10'] / M['m00'])
            cy = int(M['m01'] / M['m00'])
            area = cv2.contourArea(cnt)
            (x, y), (MA, ma), angle = cv2.fitEllipse(cnt)

            dictElem = {"cx": cx, "cy": cy, "area": area, "angle": angle, "data": cnt}
            propertyCollection.append(dictElem.copy())

    field = Field(propertyCollection, dif.shape, 4, 2)
    coll = field.getInstrumentPointCollection(120, dif.shape[0] // 3)

    allPoints = []
    if coll is None: return None

    def writeContour(c):
        list(map(lambda x: allPoints.append(x), c))

    list(map(lambda c: writeContour(c["data"][0]), coll))
    if len(allPoints) < 4: return None
    # straight bounding rectangle :
    x, y, w, h = cv2.boundingRect(np.array(allPoints))
    return x, y, w, h


paramDict = {'videoPath': r"video.avi", 'diffThreshold': 20, 'lowPixelValue': 180, 'impaintRadius': 3,
             'pause': 0, 'rectangleMovementPix': 100, 'videoDim': (960, 480),
             'morphKernel': cv2.getStructuringElement(cv2.MORPH_ELLIPSE, (5, 5))}


def prepareFrameForDiffFunction(bgrFrame):
    impainted = impaintHighlights(bgrFrame, paramDict['impaintRadius'], paramDict['lowPixelValue'])
    grayFrame = cv2.cvtColor(impainted, cv2.COLOR_BGR2GRAY)
    return grayFrame