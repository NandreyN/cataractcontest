import cv2
import numpy as np
from PIL import Image

from InstrumentDetection import SURF_data


def getShiftMatrix(dx, dy):
    return np.array([[1., 0, dx],
                     [0, 1., dy]])


def shifting(image, dx, dy):
    trMatrix = getShiftMatrix(dx, dy)
    return cv2.warpAffine(image, trMatrix, (image.shape[1], image.shape[0]))


def filterMatches(kp1, kp2, matches, ratio=0.75):
    gkp1, gkp2 = [], []
    for match in matches:
        if len(match) == 2 and match[0].distance <= match[1].distance * ratio:
            gkp1.append(kp1[match[0].queryIdx])
            gkp2.append(kp2[match[0].trainIdx])

    points1 = np.float32([kp.pt for kp in gkp1])
    points2 = np.float32([kp.pt for kp in gkp2])
    compressed = zip(points1, points2)
    return points1, points2, compressed


def smooth(firstFrame, currFrame):
    """
    Calculates displacement between current frame and the first one using SURF
    :param: firstFrame: first frame in grayscale
    :param: currFrame: current frame in grayscale
    :return: confusion vector. Frame - > (vector) -> First
    """
    firstFrame = np.array(firstFrame)
    currFrame = np.array(currFrame)

    kpFirst, desFirst = SURF_data(firstFrame, t=400)
    kpCurr, desCurr = SURF_data(currFrame, t=400)

    bf = cv2.BFMatcher(cv2.NORM_L2)
    matches = bf.knnMatch(desFirst, desCurr, k=2)
    g1, g2, pairs = filterMatches(kpFirst, kpCurr, matches, ratio=0.7)

    diff = g2 - g1  # difference between points
    if len(diff) is 0: return None

    dx, dy = np.median(diff[:, 0]), np.median(diff[:, 1])

    return shifting(currFrame, -dx, -dy), (-dx, -dy)
