import datetime

import cv2
import os
import numpy as np

from DirectoryTools import enumerateFiles
from ImageDifference import calcDiff


def SURF(gray):
    surf = cv2.xfeatures2d.SURF_create(400, extended=True)

    kp, des = surf.detectAndCompute(gray, None)
    return cv2.drawKeypoints(gray, kp, gray, flags=cv2.DRAW_MATCHES_FLAGS_DRAW_RICH_KEYPOINTS)


def SURF_data(gray, t):
    surf = cv2.xfeatures2d.SURF_create(t, extended=True)

    kp, des = surf.detectAndCompute(gray, None)
    return kp, des


def SIFT(gray):
    """
    :param image: grayscale image
    :return: image with dots detected
    """

    sift = cv2.xfeatures2d.SIFT_create()
    keyPoints = sift.detect(gray, None)
    return cv2.drawKeypoints(gray, keyPoints, gray, flags=cv2.DRAW_MATCHES_FLAGS_DRAW_RICH_KEYPOINTS)


def processSURFFolder(inputFolderPath, outputFolderPath):
    if not os.path.exists(outputFolderPath):
        os.mkdir(outputFolderPath)

    files = enumerateFiles(inputFolderPath)
    # start iterating images in input folder

    print("SURF started")
    for file in files:
        startTime = datetime.datetime.now()

        fullPath = os.path.join(inputFolderPath, file)
        img = cv2.imread(fullPath)
        cv2.imwrite(os.path.join(outputFolderPath, file), SURF(cv2.cvtColor(img, cv2.COLOR_BGR2GRAY)))
        print("Handled ", fullPath, " ,time ", datetime.datetime.now() - startTime)

    print("SURF finished")


def BFMatchImg(filePath1, filePath2, threshold):
    img1 = cv2.imread(filePath1, 0)
    img2 = cv2.imread(filePath2, 0)

    surf = cv2.xfeatures2d.SURF_create(400)
    kp1, des1 = surf.detectAndCompute(img1, None)
    kp2, des2 = surf.detectAndCompute(img2, None)

    bf = cv2.BFMatcher()
    matches = bf.match(des1, des2)
    good = []
    for m in matches:
        if m.distance <= threshold:
            good.append(m)
    img = cv2.drawMatches(img1, kp1, img2, kp2, good, img1)
    return img


# Abstract:
# We take stabilized video(video.avi , precomputed) and split it into frames, resizing is preferable
# Frame`s difference is calculated, threshold(originally on the splited video series) = 10
# Two binary difference image
# Apply handling difference function for both
# get image with array of contours which admittedly belong to the instrument
# add contour points to the common list
# ?Make bounding rectangle for contour list
# ?Extract original look of the instrument from original source

def resizeRectangle(x, y, w, h, oldshape, newshape):
    ratioX = newshape[0] / oldshape[0]
    ratioY = newshape[1] / oldshape[1]

    return int(x * ratioX), int(y * ratioY), int(w * ratioX), int(h * ratioY)


