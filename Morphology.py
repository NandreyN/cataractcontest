import  cv2

import numpy as np
from scipy.ndimage import binary_opening
from skimage.morphology import disk


def opening(img,size):
    img = np.array(img)
    opening = cv2.morphologyEx(img, cv2.MORPH_OPEN, cv2.getStructuringElement(cv2.MORPH_ELLIPSE,(size,size)))
    return np.array(opening)