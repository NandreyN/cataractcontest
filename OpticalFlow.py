import datetime
import os
import numpy as np
import sys

from skimage.measure import compare_ssim

from DirectoryTools import enumerateFilesSort, enumerateFilesRange
import cv2

from ImageSmoothing import shifting


def lucasKanadeOpticalFlow(firstFrame, secondFrame):

    feature_params = dict(maxCorners=100,
                          qualityLevel=0.3,
                          minDistance=7,
                          blockSize=7)

    lk_params = dict(winSize=(15, 15),
                     maxLevel=2,
                     criteria=(cv2.TERM_CRITERIA_EPS | cv2.TERM_CRITERIA_COUNT, 10, 0.03))


    pointsOld = cv2.goodFeaturesToTrack(firstFrame, mask=None, **feature_params)
    pointsNew, st, error = cv2.calcOpticalFlowPyrLK(firstFrame, secondFrame, pointsOld, None, **lk_params)

    good_new = pointsNew[st == 1]
    good_old = pointsOld[st == 1]
    mask = np.zeros_like(firstFrame)
    color = np.random.randint(0, 255, (100, 3))

    for i, (new, old) in enumerate(zip(good_new, good_old)):
        a, b = new.ravel()
        c, d = old.ravel()
        mask = cv2.line(mask, (a, b), (c, d), color[i].tolist(), 2)
        secondFrame = cv2.circle(secondFrame, (a, b), 5, color[i].tolist(), -1)
    img = cv2.add(secondFrame, mask)
    # new good features
    return img, good_new.reshape(-1, 1, 2)


def flow(directory, pyr_scale, levels, winsize, iterations, poly_n, poly_sigma, flags):
    files = enumerateFilesSort(directory)
    prevFramePath = files[0]
    hsv = np.zeros_like(cv2.imread(os.path.join(directory, prevFramePath)))
    hsv[..., 1] = 255

    for currFramePath in files[1:]:
        print(currFramePath)
        previousFrame = cv2.imread(os.path.join(directory, prevFramePath), 0)
        currentFrame = cv2.imread(os.path.join(directory, currFramePath), 0)

        flow = cv2.calcOpticalFlowFarneback(previousFrame, currentFrame, None,
                                            pyr_scale, levels, winsize, iterations, poly_n, poly_sigma, flags)
        mag, ang = cv2.cartToPolar(flow[..., 0], flow[..., 1])
        hsv[..., 0] = ang * 180 / np.pi / 2
        hsv[..., 2] = cv2.normalize(mag, None, 0, 255, cv2.NORM_MINMAX)
        bgr = cv2.cvtColor(hsv, cv2.COLOR_HSV2BGR)
        cv2.imwrite(os.path.join("E:\Program Files\CataractContest\FlowResult", currFramePath), bgr)

        prevFramePath = currFramePath

def getFlowVector(firstFrame,secondFrame,pyr_scale, levels, winsize, iterations, poly_n, poly_sigma, flags):
    flow = cv2.calcOpticalFlowFarneback(firstFrame, secondFrame, None,
                                            pyr_scale, levels, winsize, iterations, poly_n, poly_sigma, flags)
    vect = np.average(flow,axis=(0,1)) #???
    return vect

cap = cv2.VideoCapture(r'video.avi')
ret, first = cap.read()
first = cv2.cvtColor(first, cv2.COLOR_BGR2GRAY)
#first = cv2.equalizeHist(first)

firstFrameForComparasion = first

video = cv2.VideoWriter('diffNotEqualizedNeigh.avi',cv2.VideoWriter_fourcc(*'DIVX'),20.0,(960,480),isColor=False)

while ret:
    ret,second = cap.read()
    try:
        second = cv2.cvtColor(second, cv2.COLOR_BGR2GRAY)
    except Exception:
        video.release()
        cap.release()
        sys.exit(1)

    #second = cv2.equalizeHist(second)

    #bgr,points =lucasKanadeOpticalFlow(first,second)
    score, diff = compare_ssim(first, second, full=True)
    diff = (diff * 255).astype("uint8")

    cv2.imshow("flow", diff)
    video.write(diff)

    k = cv2.waitKey(30) & 0xff
    if k == 27:
        break
    first = second.copy()
video.release()
cap.release()