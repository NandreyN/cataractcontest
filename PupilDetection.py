import datetime

import cv2
import numpy
from PIL import Image, ImageDraw
from matplotlib import patches
from matplotlib.pyplot import imshow, show, plot, figure, subplots, hist, imsave
from numpy import histogram
from numpy.ma import array
import os
from Binarization import threshold, adaptive, otsu
from DirectoryTools import enumerateFilesFullPath, enumerateFiles
from DistanceMap import createMap, dist
from HistTools import showHistogram
from Morphology import opening
from borderDetectors import borders


def getAverageBrightness(crop):
    return numpy.sum(numpy.sum(crop, axis=0)) / (crop.shape[0] * crop.shape[1])

def detect(img, n=30, showIm=None):
    """
    :param img: PIL Image to be handled
    :param n: number of cells per axes
    :param show: currently does nothing
    :return: returns image with circle mask around a pupil
    """
    # img = img.convert('L')
    """figure()
    histog, e = histogram(array(img).flatten(), bins=256)
    showHistogram(histog,e)"""
    raise NotImplementedError
    opencvImage = cv2.cvtColor(array(img), cv2.COLOR_RGB2GRAY)
    opencvImage = cv2.equalizeHist(opencvImage)

    opencvImage = cv2.cvtColor(opencvImage, cv2.COLOR_GRAY2RGB)
    img = Image.fromarray(opencvImage)

    draw = ImageDraw.Draw(img)
    # width and height of each cell
    szX, szY = img.size[0] // n, img.size[1] // n
    img = array(img)

    max = 256.
    maxRect = ()

    for i in range(n):
        for j in range(n):
            lt = (i * szX, j * szY)
            rb = ((i + 1) * szX, (j + 1) * szY)
            crop = img[lt[1]:rb[1], lt[0]:rb[0]]
            average = getAverageBrightness(crop)
            if average < max:
                max = average
                maxRect = (lt, rb)
    del draw

    corner = (maxRect[0][0], maxRect[1][1])
    fig, ax = subplots(1)

    # Display the image
    ax.imshow(img, cmap="gray")

    # Create a Rectangle patch
    rect = patches.Rectangle(corner, rb[0] - lt[0], rb[1] - lt[1], linewidth=1, edgecolor='r', facecolor='none')

    # Add the patch to the Axes
    ax.add_patch(rect)
    show()


def detectCircles(img):
    """
    :param img: grayscale image
    :return:
    """
    img = array(img)
    circles = cv2.HoughCircles(img, cv2.HOUGH_GRADIENT, 1, 200, param1=255, param2=10, minRadius=img.shape[1] // 8,
                               maxRadius=img.shape[1] // 6)

    circles = filterCircles(circles[0, :], img.shape)
    if circles is None: return img

    img = cv2.cvtColor(img, cv2.COLOR_GRAY2RGB)
    R, x, y = int(circles[0][0][2]), int(circles[0][0][0]), int(circles[0][0][1])
    cv2.circle(img, (x, y), R, (255, 0, 0), 3, 8, 0)

    for circle in circles[1:]:
        Radius, x, y = int(circle[0][2]), int(circle[0][0]), int(circle[0][1])
        cv2.circle(img, (x, y), 1, (255, 255, 255), -1, 8, 0)
        cv2.circle(img, (x, y), Radius, (255, 255, 255), 3, 8, 0)
    return img


def filterCircles(circles, shape):
    if circles is None: return None

    centreX, centreY = shape[0] // 2, shape[1] // 2
    distances = list(map(lambda args: float(dist((centreX, centreY), (args[1][1], args[1][0])) + args[0] * 150),
                         enumerate(circles)))

    zipped = list(zip(circles, distances))
    zipped.sort(key=lambda x: x[1])

    if len(zipped) is 0: return None
    return zipped


def processPupilDetection(filePath):
    """
    :param filePath: string that represents full path to the image processed
    :return: result image with pupil detected or not
    """
    # mapIm = createMap(Image.open(filePath))
    # binary = threshold(mapIm, 60)
    # binary = adaptive(array(Image.open(filePath)),5,101,3)
    # binary = opening(array(Image.open(filePath).convert('L')),30)
    img = array(Image.open(filePath).convert('L'))
    # open = cv2.morphologyEx(img, cv2.MORPH_OPEN, cv2.getStructuringElement(cv2.MORPH_ELLIPSE, (30, 30)))
    # close = cv2.morphologyEx(img, cv2.MORPH_CLOSE, cv2.getStructuringElement(cv2.MORPH_ELLIPSE, (30, 30)))
    # grayscale = numpy.array(list(map(lambda x: x * 255, array(binary)))).astype(numpy.int8)
    # grayscale = Image.fromarray(grayscale).convert("L")
    detected = detectCircles(numpy.bitwise_not(img))
    return detected


raise NotImplementedError("Not implemented processPupilDetection function")

inputFolderPath = "E:\\Program Files\\CataractContest\\OpeningAdaptive"
outputFolderPath = "E:\\Program Files\\CataractContest\\CirclesAdaptive"
if not os.path.exists(outputFolderPath):
    os.mkdir(outputFolderPath)

files = enumerateFiles(inputFolderPath)
# start iterating images in input folder

print("Started")
for file in files:
    startTime = datetime.datetime.now()

    fullPath = os.path.join(inputFolderPath, file)
    outIm = Image.fromarray(processPupilDetection(fullPath))
    outIm.save(os.path.join(outputFolderPath, file))
    print("Handled ", fullPath, " ,time ", datetime.datetime.now() - startTime)

print("Finished")
