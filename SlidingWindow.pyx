import matplotlib.pyplot as plt
import cv2
import cython
import numpy as np
import math
cimport numpy as np
import numpy.linalg as la
from libc.math cimport sqrt

cpdef double soft_cosine(np.ndarray[np.int32_t, ndim=1] a,np.ndarray[np.int32_t, ndim=1] b, np.ndarray[np.uint8_t, ndim=2] matrix):
    """
    https://en.wikipedia.org/wiki/Cosine_similarity
    :param matrix:
    :param a: array
    :param b: array
    :return: cosine
    """
    cdef double soft_cosine_value = 0.
    cdef double s1 = 0.
    cdef double s2 = 0.
    cdef double s3 = 0.

    for row in range(256):
            for col in range(256):
                s1 += a[row] * b[col] * matrix[row, col]

    for row in range(256):
            for col in range(256):
                s2 += a[row] * a[col] * matrix[row, col]
    s2 = sqrt(s2)

    for row in range(256):
            for col in range(256):
                s3 += b[row] * b[col] * matrix[row, col]
    s3 = sqrt(s3)

    return s1 / (s2 * s3)


cpdef is_zero(int[:] vect):
    for i in range(len(vect)):
        if vect[i] != 0:
            return None
    return True

cpdef np.ndarray[np.int32_t, ndim=1] get_hist(np.ndarray[np.uint8_t, ndim=2] image):
    h, e = np.histogram(image, bins=256)  # h = histogram
    return h

cdef double get_measure(np.ndarray[np.int32_t,ndim=1] src,np.ndarray[np.int32_t,ndim=1] current):
    cdef double norm = 0.0
    for i in range(len(src)):
        norm += (abs(src[i] - current[i]))**2
    norm = norm ** 0.5
    return norm


cdef double get_measure2(np.ndarray[np.int32_t,ndim=1] src,np.ndarray[np.int32_t,ndim=1] current):
    cdef double sum = 0.0
    for i in range(len(src)):
        sum += abs(src[i] - current[i])

    return sum

class Container:
    def __init__(self, np.ndarray[np.uint8_t, ndim=2] src_image, int COL, int ROW, int window_size):
        self._dict = {}
        assert window_size % 2 != 0

        cdef int i1
        cdef int i2
        cdef int j1
        cdef int j2
        cdef int half_size = window_size // 2
        cdef int allowed_rows_top = half_size
        cdef int allowed_rows_bottom = ROW - half_size
        cdef int allowed_col_left = half_size
        cdef int allowed_col_right = COL - half_size
        cdef np.ndarray[np.int32_t, ndim=1] src_hist
        cdef np.ndarray[np.uint8_t, ndim=2] image_crop

        print(allowed_rows_bottom, allowed_rows_top)
        print(allowed_col_left, allowed_col_right)

        for i in range(allowed_rows_top,allowed_rows_bottom):
            for j in range(allowed_col_left, allowed_col_right):
                i1 = i - half_size
                i2 = i + half_size
                j1 = j - half_size
                j2 = j + half_size
                image_crop = src_image[i1:i2, j1:j2]

                src_hist = get_hist(image_crop)
                if src_hist is not None:
                    self._dict[(i,j)] = src_hist
                    print(i,j)
                else:
                    print("Init None, ", (i,j))

        self._matrix = np.zeros((256,256),dtype=np.uint8)
        for row in range(256):
            for col in range(256):
                self._matrix[row, col] = 255 - abs(row - col)


    def slide(self, np.ndarray[np.uint8_t, ndim=2] current, allowed_columns, allowed_rows, int window_size, double threshold,
              int step):
        # allowed_x and allowed_y are tuples which contain range of pixels being handled
        assert window_size % 2 != 0

        cdef int i1
        cdef int i2
        cdef int j1
        cdef int j2
        cdef int half_size = window_size // 2

        cdef np.ndarray[np.uint8_t, ndim=2] result_image = np.zeros_like(current)
        cdef np.ndarray[np.uint8_t, ndim=2] image_crop
        cdef np.ndarray[np.int32_t, ndim=1] current_hist
        cdef np.ndarray[np.int32_t, ndim=1] src_hist


        cdef int allowed_col_left = allowed_columns[0] + half_size
        cdef int allowed_col_right = allowed_columns[1] - half_size
        cdef int allowed_rows_top = allowed_rows[0] + half_size
        cdef int allowed_rows_bottom = allowed_rows[1] - half_size

        cdef double angle

        if allowed_col_left - allowed_col_right>= 0 or allowed_rows_top - allowed_rows_bottom>= 0:
            print("window size wrong")
            return None


        for i in range(allowed_rows_top,allowed_rows_bottom,step):
            for j in range(allowed_col_left,allowed_col_right,step):
                i1 = i - half_size
                i2 = i + half_size
                j1 = j - half_size
                j2 = j + half_size
                image_crop = current[i1:i2, j1:j2]

                current_hist = get_hist(image_crop)
                if (i,j) in self._dict:
                    src_hist = self._dict[(i,j)]
                else:
                    src_hist = None
                    print("src_hist is None", (i,j))

                if (    current_hist is None or \
                        src_hist is None or\
                        is_zero(current_hist) is True or\
                        is_zero(src_hist) is True):
                    continue

                angle = soft_cosine(current_hist, src_hist,self._matrix)

                result_image[i,j] = 255 if angle < threshold else 0
        return result_image
