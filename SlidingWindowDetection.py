import cv2
import numpy as np
import time

import SlidingWindow
from HighlightHandler import impaintHighlights

from ImageSmoothing import smooth

INPUT_VIDEO_PATH = r"E:\Program Files\CataractContest\test01.mp4"
WINDOW_WIDTH = 55
THRESHOLD = np.cos(np.pi / 7)
STEP = 4
START_WITH_FRAME = 832
BLUR_RATIO = 7

if __name__ == "__main__":
    """ Description block
    Target of that module is to detect probable instrument presence using histogram difference data.
    Starting with getting shift between first frame in the video and the current frame
    That we start inspecting image with a sliding windows, getting histograms for corresponding regions of both images 
    Calculate angles between histograms and compare it with some kind of threshold 
    Use shift data to filter "black stripes" on the current image
    """
    assert WINDOW_WIDTH % 2 != 0

    video_cap = cv2.VideoCapture(INPUT_VIDEO_PATH)
    assert video_cap.isOpened()

    read_status, FIRST_FRAME = video_cap.read()
    assert read_status

    FIRST_FRAME = cv2.resize(FIRST_FRAME, (960, 480))
    FIRST_FRAME = cv2.cvtColor(FIRST_FRAME, cv2.COLOR_BGR2GRAY)
    FIRST_FRAME = cv2.equalizeHist(FIRST_FRAME)
    FIRST_FRAME = cv2.GaussianBlur(FIRST_FRAME, (BLUR_RATIO, BLUR_RATIO), 0)
    idx = 0

    print("Initialization started, this make take time")
    container = SlidingWindow.Container(FIRST_FRAME, FIRST_FRAME.shape[1], FIRST_FRAME.shape[0], WINDOW_WIDTH)
    print("Initialization finished")

    while read_status:
        if idx < START_WITH_FRAME:
            read_status, current_frame = video_cap.read()
            idx += 1
            continue
        read_status, current_frame = video_cap.read()
        current_frame = cv2.resize(current_frame, (960, 480))
        current_frame = cv2.cvtColor(current_frame, cv2.COLOR_BGR2GRAY)
        current_frame = cv2.equalizeHist(current_frame)
        current_frame = cv2.GaussianBlur(current_frame, (BLUR_RATIO,BLUR_RATIO), 0)
        shifted_image, (dx, dy) = smooth(FIRST_FRAME, current_frame)

        wnd_centre_x, wnd_centre_y = WINDOW_WIDTH, WINDOW_WIDTH
        allowed_x = [0, current_frame.shape[1]]
        allowed_y = [0, current_frame.shape[0]]

        if dx <= 0:  # stripe will be at the right
            allowed_x[1] -= int(abs(dx))
        else:
            allowed_x[0] += int(dx)

        if dy <= 0:  # stripe will be at the bottom
            allowed_y[1] -= int(abs(dy))
        else:
            allowed_y[0] += int(dy)

        result_image = np.array(container.slide(shifted_image, allowed_x, allowed_y, WINDOW_WIDTH, THRESHOLD, STEP))
        print(
            "Frame {}, allowed_x: {}, allowed_y: {}, shape : {}".format(idx, allowed_x, allowed_y, result_image.shape))

        cv2.imshow("flow", np.hstack((cv2.resize(result_image, (480, 360)), cv2.resize(shifted_image, (480, 360)))))
        k = cv2.waitKey(30) & 0xff
        if k == 27:
            break

        idx += 1
    video_cap.release()
