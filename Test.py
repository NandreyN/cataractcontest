'''def p(x):
    return x


def q(x):
    return -(x ** 2 + 1)


def f(x):
    return -(x**4) + x**2 + 2


h = 0.1
n = 1 / h
for k in range(1,int(n)):
    y_k_min_1 = 1 - (h/2)*p(k*h)
    y_k = (h**2)*q(k*h)
    y_k_plus_1 = 1 + (h/2)*p(k*h)
    print("line {}: yk-1 = {}, yk = {}, yk+1= {}".format(k,y_k_min_1,y_k,y_k_plus_1))

print("Y:")
for i in range(1, int(n)):
    fk = (h**2) * f(i*h)
    print("k = {}, F{} = {}".format(i, i,fk))'''

a = [0,-0.995, -0.99, -0.985, -0.98, -0.975, -0.97, -0.965, -0.96, -0.955, -0.2] # leading zero
b = [-1, -1.005, -1.01, -1.015, -1.02, -1.025, -1.03, -1.035, -1.04, -1.045]
c = [-0.8, -0.0101, -0.0104, -0.0109, -0.0116, -0.0125, -0.0136, -0.0149, -0.0164, -0.0181, 0.3]
f = [0.5, 0.020099, 0.020384, 0.020819, 0.021344, 0.021875, 0.022304, 0.022499, 0.022304, 0.021539, 0.5]
N = len(b)

'''a = [0, -3,-1,-1]
b = [-3,-1,2]
c = [5,6,4,-3]
f = [8,10,3,-2]
N = len(b)'''

alpha = [0.] * (N + 1)
beta = [0.] * (N + 1)

alpha[1] = (b[0] / c[0])
beta[1] = (f[0] / c[0])

for i in range(2, N + 1):
    alpha[i] = b[i - 1] / (c[i - 1] - (a[i - 1] * alpha[i - 1]))
    beta[i] = (f[i - 1] + (a[i - 1] * beta[i - 1])) / (c[i - 1] - (a[i - 1] * alpha[i - 1]))

beta.append((f[N] + (a[N] * beta[N])) / (c[N] - (a[N] * alpha[N])))

y = [0.] * (N+1)
y[N] = beta[N + 1]
for k in reversed(range(0, N)):
    y[k] = ((alpha[k + 1] * y[k+1]) + beta[k + 1])

for i,r in enumerate(y):
    print("y{} = {}".format(i,r))

for i in range(1, N+1):
    print("Alpha{}: {}".format(i,alpha[i]))

for i in range(1, N + 2):
    print("Beta{}: {}".format(i, beta[i]))
