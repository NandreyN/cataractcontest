import os

import cv2
from PIL import Image
from matplotlib.pyplot import imshow, show

from DirectoryTools import enumerateFilesRange

def track(fileSeq):
    """
    :param fileSeq:
    :return:
    """
    if len(fileSeq) is 0: raise ValueError("Empty folder")

    tracker = cv2.Tracker_create("MIL")
    if tracker is None: raise ValueError("Cannot create tracker")

    firstFrame = cv2.imread(fileSeq[0],0)
    box = (720,105,600,600)
    ok = tracker.init(firstFrame, box)
    if not ok: raise Exception("Cannot init tracker")

    for file in fileSeq[1:]:
        currentFrame = cv2.imread(file,0)
        currentFrame = cv2.equalizeHist(currentFrame)

        ok,box = tracker.update(currentFrame)
        if ok:
            p1 = (int(box[0]), int(box[1]))
            p2 = (int(box[0] + box[2]), int(box[1] + box[3]))
            cv2.rectangle(currentFrame, p1, p2, (0, 0, 255))
        cv2.imshow("Tracking", cv2.resize(currentFrame,(960,480)))
        k = cv2.waitKey(1) & 0xff
        if k == 27: break


dir = "E:\Program Files\CataractContest\Test"
track([os.path.join(dir, f) for f in enumerateFilesRange(dir, 1, 12328)])
