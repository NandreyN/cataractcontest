import cv2

import numpy
import scipy
from PIL import Image
from collections import defaultdict
from matplotlib.pyplot import imshow, show, figure, title, subplot, subplots
from scipy import ndimage

from Binarization import otsu, adaptive, threshold


def func(x):
    """
    # Switch operator replacement for calling edge detection method
    :param x: mode
    :return:function
    """
    return {
        "sobel": sobel,
        "canny": canny,
        "laplacian":laplacian,
    }[str.lower(x)]

def canny(im):
    res = None
    try:
        res = cv2.Canny(im,150,255)
    except TypeError:
        print("Try to pass grayscale image")
    return res

def laplacian(im):
    lapl = None
    try:
        lapl = cv2.Laplacian(im, cv2.CV_8U)
    except TypeError:
        print("Try to pass grayscale image")
    return lapl


def sobel(im):
    sob = None
    try:
        sob = cv2.Sobel(im, cv2.CV_8U, 1, 1, ksize=5)
    except TypeError:
        print("Try to pass grayscale image")
    return sob


def borders(im, mode,sigma = 5):
    """
    Uses Otsu thresholding and Canny/Sobel for border detecting
    :param im: Image data
    :param sigma: Sigma for GaussianBlur func
    :param mode: edge detection mode : "sobel", "canny"
    :return: image with borders been detected
    """
    #bin = otsu(im, sigma)
    bin = threshold(im,90)
    # binarized image
    f = func(mode)
    edges = f(bin)
    return edges