import cv2
import cython
import numpy as np
cimport numpy as np


cpdef unsigned char [:,:] getDiff(unsigned char[:,:] first, unsigned char[:,:] second, int threshold):
    cdef np.ndarray[np.uint8_t, ndim =2] diffImage = np.empty_like(first, dtype=np.uint8)

    cdef Py_ssize_t i, j
    cdef int sum
    cdef double averageFirst
    cdef double averageSecond

    for i in range(diffImage.shape[0]):
        for j in range(diffImage.shape[1]):
            if 0 < i < diffImage.shape[0] - 1 and 0 < j < diffImage.shape[1] - 1:
                sum = first[i-1,j-1] + first[i-1,j] + first[i-1,j+ 1] + first[i,j-1] + first[i,j + 1] + first[i+ 1,j - 1] +first[i + 1,j] + first[i+1,j + 1]
                averageFirst = sum / 8

                sum = second[i-1,j-1] + second[i-1,j] + second[i-1,j+ 1] + second[i,j-1] + second[i,j + 1] + second[i+ 1,j - 1] + second[i + 1,j] + second[i+1,j + 1]
                averageSecond = sum / 8

                if (abs(averageFirst - averageSecond) <= threshold):
                    diffImage[i,j] = 0
                else:
                    diffImage[i,j] = 255

            else:
                diffImage[i,j] = 0
    return diffImage
