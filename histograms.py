import os.path
from os.path import  join
from PIL import Image
from pylab import *
import HistTools
import numpy
import ImageCut
from DirectoryTools import enumerateFiles, enumerateFilesFullPath
from tsne import tsne


def averageHistorgramsExec():
    directoriesToHandle = ["E:\Program Files\CataractContest\ImageSeries\Eye",
                       "E:\Program Files\CataractContest\ImageSeries\Instrument1",
                       "E:\Program Files\CataractContest\ImageSeries\Instrument2",
                       "E:\Program Files\CataractContest\ImageSeries\Instrument3",
                       "E:\Program Files\CataractContest\ImageSeries\Pupil"]

    for directory in directoriesToHandle:
        data,e = HistTools.getAvHistForFolder(directory)
        for i in range(3):
            outDirPath = join(directory, "Hist")
            if (not os.path.exists(outDirPath)):
                os.makedirs(outDirPath)

            HistTools.saveHistogram(data[i],i,outDirPath)
def cutImagesExec(sizeX,sizeY, max ):
    directoriesToHandle = ["E:\Program Files\CataractContest\ImageSeries\Class1",
                           "E:\Program Files\CataractContest\ImageSeries\Class2",
                           "E:\Program Files\CataractContest\ImageSeries\Class3",
                           "E:\Program Files\CataractContest\ImageSeries\Class4",
                           "E:\Program Files\CataractContest\ImageSeries\Class5"]

    for p in directoriesToHandle:
        ImageCut.cutImagesInFolder(p, join(p,"Cut"),sizeX,sizeY,max)

def tsneExec():
    directoriesToHandle = ["E:\Program Files\CataractContest\ImageSeries\Class1\Cut",
                           "E:\Program Files\CataractContest\ImageSeries\Class2\Cut",
                           "E:\Program Files\CataractContest\ImageSeries\Class3\Cut",
                           "E:\Program Files\CataractContest\ImageSeries\Class4\Cut",
                           "E:\Program Files\CataractContest\ImageSeries\Class5\Cut"]
    fullList = []
    types = []
    classId = 0
    for x in directoriesToHandle:
        f = enumerateFilesFullPath(x)
        fullList += f
        classId += 1
        types += [str(classId) for i in range(len(f))]

    tsne(fullList, types)
tsneExec()