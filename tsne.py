from PIL import Image
from sklearn.manifold import TSNE
import numpy as np
import PIL
import matplotlib.pyplot as plt
import matplotlib.cm as cm

import HistTools
from HistTools import concatImageHistograms

def tsne(objPaths, typesVector):
    #objPaths is a list of paths to files to be handled
    print("Getting histograms")

    dataSet = [np.array(concatImageHistograms(obj)) for obj in objPaths]
    print("Starting T-SNE")

    projection = TSNE().fit_transform(dataSet)

    print("T-SNE finished")
    visualize(projection, typesVector)

def visualize(tsneData, types):
    #use scatter plot to show vectors` distribution
    x = [tup[0] for tup in tsneData]
    y = [tup[1] for tup in tsneData]
    unique = list(set(types))
    colors = cm.rainbow(np.linspace(0,1, len(unique)))

    plt.figure()
    for i in range(len(x)):
        plt.scatter(x=x[i], y = y[i], color=colors[int(types[i]) - 1], s=7)

    plt.show()
